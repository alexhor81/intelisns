//js for intelis

//to top

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if ((document.body.scrollTop > 20 || document.documentElement.scrollTop > 20 ) && document.body.clientWidth  > 540) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// Google map

// var map;
// function initMap() {
//     var myLatLng= {lat: 45.2355168, lng: 19.8271089};

//     map = new google.maps.Map(document.getElementById('map'), {
//         center: myLatLng,
//         zoom: 14
//     });
//     var marker = new google.maps.Marker({
//         position: myLatLng,
//         map: map,
//         draggable: true,
//         animation: google.maps.Animation.DROP,
//         title: 'Intelis ICT'
//     });
//     //marker.addListener('click', toggleBounce);

//     //Infowindow
//     var contentString = '<div id="content">'+
//         '<div id="siteNotice">'+
//         '</div>'+
//         '<h1 id="firstHeading" class="firstHeading">Intelis ICT</h1>'+
//         '<div id="bodyContent">'+
//         '<p>Prodavnica racunara</p>';
//     var infowindow = new google.maps.InfoWindow({
//         content: contentString
//     });
//     marker.addListener('click', function() {
//         infowindow.open(map, marker);
//     });

//     function toggleBounce() {
//         if (marker.getAnimation() !== null) {
//             marker.setAnimation(null);
//         } else {
//             marker.setAnimation(google.maps.Animation.BOUNCE);
//         }
//     }
// }


/*---------------------
     Leaflet Maps
  --------------------- */
   // Creating map options 
        var marker_text = "Intelis ICT"; 
         var mapOptions = {
            center: [45.235537, 19.827914],
            zoom:  20
         }
         // Creating a map object
         var map = new L.map('map', mapOptions);
         
         // Creating a Layer object
        // var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([45.235537, 19.827914]).addTo(map)
        .bindPopup(marker_text)
        .openPopup();
         // Adding layer to the map
         map.addLayer(layer);
         
         // Creating a Marker
         var markerOptions = {
            title: "Intelis ICT",
            clickable: true,
            draggable: false
         } 
         
         // Adding marker to the map
         marker.addTo(map);


  /**
    * Disable right-click of mouse, F12 key, and save key combinations on page
    * By Arthur Gareginyan (arthurgareginyan@gmail.com)
    * For full source code, visit http://www.mycyberuniverse.com
    */
  window.onload = function() {
    document.addEventListener("contextmenu", function(e){
      e.preventDefault();
    }, false);
    document.addEventListener("keydown", function(e) {
    //document.onkeydown = function(e) {
      // "I" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      // "J" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      // "S" key + macOS
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      // "U" key
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      // "F12" key
      if (event.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);
    function disabledEvent(e){
      if (e.stopPropagation){
        e.stopPropagation();
      } else if (window.event){
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }
  };       