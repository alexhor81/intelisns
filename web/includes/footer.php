<footer class="container">
    <div class="">
        <div class="row">
            <div class="text col-sm-12 col-lg-6 mt-sm-0 mt-lg-4">2018 © Intelis. All rights reserved</div>
            <div class="col-sm-12 col-lg-6 text-lg-right mt-sm-0 mt-lg-2">
                <ul class="list-unstyled list-inline">
                    <li class="list-inline-item"><a href="https://twitter.com/jetimpex"><i
                            class="fab fa-twitter-square fa-2x"></i></a></li>
                    <li class="list-inline-item"><a href=""><i class="fab fa-facebook fa-2x"></i></a></li>
                    <li class="list-inline-item"><a href=""><i class="fab fa-google-plus-square fa-2x"></i></a></li>
                    <li class="list-inline-item"><a href=""><i class="fab fa-linkedin fa-2x"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- GoTop link -->
<div class="back-to-top" onclick="topFunction()" id="myBtn" >
    <a href="#" class=”back-to-top__link ”>

        <i class="fas fa-arrow-up fa-1x"></i>

    </a>
</div>
<!-- jQuery library -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<!--map-->
 
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!-- My JS file -->
<script src="js/intelisns.js"></script>
 

</body>
</html>