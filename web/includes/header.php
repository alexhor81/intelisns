<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
     <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
   <!-- Make sure you put this AFTER Leaflet's CSS -->
 <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>        
    <link rel="stylesheet" href="css/styles.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body oncontextmenu="return false;">
<section class="container bg-secondary">
    <div class="row">

        <div class="col-sm-12 col-md-8 text-center text-lg-left mt-3">
            <span><i class="fas fa-phone hoverEF"></i> Kontakt telefon: </span>
            <span class="hoverEF"> 063 7754880 </span>
        </div>

        <div class="col-sm-12 col-md-4">
            <ul class="list-inline mt-3 text-center text-lg-right">
                <li class="list-inline-item"><a  class="social-icon text-xs-center" target="_blank" href="#"><i
                        class="fab fa-twitter-square fa-2x hoverEF"></i></a></li>
                <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="https://www.facebook.com/IntelisICT/"><i
                        class="fab fa-facebook-square fa-2x hoverEF"></i></a></li>
                <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#"><i
                        class="fab fa-google-plus-square fa-2x hoverEF"></i></a></li>
                <li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="https://www.linkedin.com/in/ognjen-vukovic-94763710/?originalSubdomain=rs"><i
                        class="fab fa-linkedin fa-2x hoverEF"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<section class="container sectionFirst">
    <div class="navigation">
        <nav class="navbar navbar-light  navbar-expand-md row">
            <div class="col-12 col-md-4">
                <a class="navbar-brand" href="#"><img src="../assets/LOGO.png" class="img-fluid"></a>
                <button class="navbar-toggler bg-primary" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse col-12  col-md-8" id="navbarNav">
                <ul class="navbar-nav justify-content-around row">
                    <li class="nav-item py-md-1 ml-2"><a href="index" class="nav-link text-uppercase font-weight-bold ml-md-5">Home</a></li>
                    <li class="nav-item py-md-1 ml-2"><a href="contact" class="nav-link text-uppercase font-weight-bold ml-md-5">Kontakt</a>
                    </li>
                    <li class="nav-item py-md-1 ml-2"><a href="about" class="nav-link text-uppercase font-weight-bold ml-md-5">O Nama</a>
                    </li>
                    <!--<li class="nav-item py-md-1 ml-2"><a href="location.html" class="nav-link text-uppercase font-weight-bold ml-md-5">Lokacija</a>-->
                    <!--</li>-->
                </ul>
            </div>
        </nav>
    </div>
</section>