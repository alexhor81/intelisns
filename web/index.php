<?php
/**
 * Requesting the header from includes folder
 */
require_once("includes/header.php");

?>
<section class="container sectionSecond  d-md-block">

</section>
<section class="container sectionThird">
    <div class="row pt-5 pl-3">
        <div class="col-12 col-md-4 box">
            <h2 class="h4">Računarska oprema</h2>
            <img src="../assets/keyboard.jpg" alt="" class="img-fluid">

            <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, aperiam, architecto aspernatur dolore, facilis impedit itaque maiores maxime molestias odit optio quasi temporibus vitae? Quidem?</p>
        </div>
        <div class="col-12 col-md-4 box">
            <h2 class="h4">Mreže</h2>

            <img src="../assets/imgix-391813-unsplash.jpg" alt="" class="img-fluid">

            <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, aperiam, architecto aspernatur dolore, facilis impedit itaque maiores maxime molestias odit optio quasi temporibus vitae? Quidem?</p>
        </div>
        <div class="col-12 col-md-4 box">
            <h2 class="h4 ">Licencirani Software</h2>
            <img src="../assets/markus-spiske-771011-unsplash.jpg" alt="" class="img-fluid">
            <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, aperiam, architecto aspernatur dolore, facilis impedit itaque maiores maxime molestias odit optio quasi temporibus vitae? Quidem?</p>

        </div>
    </div>
</section>

<section id="links" class="links container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-3">
                <img src="../assets/LOGO.png" alt="" class="img-fluid mt-3 mb-3">
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet delectus dicta, dignissimos dolore est excepturi fugiat ipsam necessitatibus nesciunt nihil optio, perferendis recusandae tenetur totam velit voluptas voluptatem voluptates voluptatibus.</p>
            </div>
            <div class="col-sm-12 col-lg-3">
                <h4 class="mt-5 mb-3">O nama</h4>
                <ul class="list-unstyled">
                    <li class="mt-1 mb-1"><a href="#">Nas Tim</a></li>
                    <li class="mt-1 mb-1"><a href="#">Nasi Proejkti</a></li>
                    <li class="mt-1 mb-1"></li>
                    <li class="mt-1 mb-1"></li>
                </ul>
            </div>
            <div class="col-sm-12 col-lg-3">
                <h4 class="mt-5 mb-3">Partneri</h4>
                <ul class="list-unstyled">
                    <li><a href="#">HP</a></li>
                    <li><a href="#">Microsoft</a></li>
                    <li><a href="#">IBM</a></li>
                    <li><a href="#"></a></li>
                </ul>
            </div>
            <div class="col-sm-12 col-lg-3">
                <p class="mt-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, ipsum!</p>
                <ul class="list-unstyled">
                    <li class="p-2"><i class="fas fa-map-marker-alt"></i> <a href="https://www.google.com/maps/place/Intelis+ICT/@45.2355168,19.8271089,18.25z/data=!4m13!1m7!3m6!1s0x475b101f56e00423:0x7355baf586a6e106!2zMTMwMCDQutCw0L_Qu9Cw0YDQsCAxOC0yNCwg0J3QvtCy0Lgg0KHQsNC0IDIxMDAw!3b1!8m2!3d45.2356066!4d19.8282343!3m4!1s0x475b1021e2851897:0x44aa36f05eb5aa79!8m2!3d45.2354658!4d19.827921?hl=sr">1300 kaplara 18-24, Novi Sad 21000</a>
                    </li>
                    <li class="p-2"><i class="fas fa-envelope-open"></i> <a href="index.html"> Website</a> </li>
                </ul>
            </div>
        </div>
    </div>

</section>

<!-- END OF SECTION LINKS AND BEGINING OF FOOTER -->

<?php
/**
 * Requesting the footer from includes folder
 */
require_once("includes/footer.php");

?>