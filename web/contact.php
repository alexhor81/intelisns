<?php
/**
 * Requesting the header from includes folder
 */
require_once("includes/header.php");

?>
<nav aria-label="breadcrumb" class="container">
    <ol class="breadcrumb bg-transparent">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Contact</li>
    </ol>
</nav>





<!--SECTION MAP-->

<section class="my-map container bg-secondary" id="my-map">

    <div id="map">

    </div>
</section>


<!--SECTION CONTACT FORM-->
<section class="contact-form">
    <div class="container">
        <div class="row mt-5">
            <div class="col-12 col-md-5">
 
                <h4 class="mb-5">Kontakt</h4>
                <ul class="list-unstyled">
                    <li>

                        <address>
                            <strong class="text-uppercase">Adresa:</strong> <a
                                href="https://www.google.com/maps/place/Intelis+ICT/@45.2355168,19.8271089,18.25z/data=!4m13!1m7!3m6!1s0x475b101f56e00423:0x7355baf586a6e106!2zMTMwMCDQutCw0L_Qu9Cw0YDQsCAxOC0yNCwg0J3QvtCy0Lgg0KHQsNC0IDIxMDAw!3b1!8m2!3d45.2356066!4d19.8282343!3m4!1s0x475b1021e2851897:0x44aa36f05eb5aa79!8m2!3d45.2354658!4d19.827921?hl=sr">1300 kaplara 18-24, Novi Sad 21000</a>
                        </address>

                    </li>
                    <li class="mb-2">
                        <strong class="text-uppercase">
                            Telefon:
                        </strong>
                        <a href="tel:8001230045"> 063 7754880</a>
                        <br>
                    </li>
                    <li>
                        <strong class="text-uppercase">
                            Radno vreme:
                        </strong>
                        <time>
                            Ponedeljak-Petak: 	09:00–16:00
                        </time>
                    </li>
                </ul>
            </div>
            <?php 
if(isset($_POST['submit']) && !empty($_POST['submit'])):
    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
        //your site secret key
        $secret = '6LemsnMUAAAAAMV2O7Ot0ufZTaA07w2z8_GgJyBT';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success):
            //contact form submission code
            $name = !empty($_POST['first_name'])?$_POST['first_name']:'';
            $last_name = !empty($_POST['last_name'])?$_POST['last_name']:'';
            $email = !empty($_POST['email'])?$_POST['email']:'';
            $message = !empty($_POST['message'])?$_POST['message']:'';
            
            $to = 'stevan314@gmail.com';
            $subject = 'Nova poruka sa Intelis websajta';
            $htmlContent = "
                <h1>Detalji pošaljioca</h1>
                <p><b>Ime: </b>".$name."</p>
                <p><b>Prezime: </b>".$last_name."</p>
                <p><b>Email: </b>".$email."</p>
                <p><b>Poruka: </b>".$message."</p>
            ";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers
            $headers .= 'From:'.$name.' <'.$email.'>' . "\r\n";
            //send email
            @mail($to,$subject,$htmlContent,$headers); 
            ?>
            <script>
            alert("Poruka poslata!");
            </script>
            <?php 
        else:
            ?>
            <script>
            alert("Pokušaj ponovo!!");
            </script> 
            <?php
        endif;
    else:
        ?>
             <script>
            alert("Klikni reCAPTCHA!!");
            </script>  
    <?php    
    endif;
else:
    $errMsg = '';
    $succMsg = '';
endif;
            ?>
<div class="col-12 col-md-7">
<h4 class="text-uppercase ">Kontakt forma:</h4>
<form action="" method="post">
First Name: <input type="text" name="first_name"><br>
Last Name: <input type="text" name="last_name"><br>
Email: <input type="text" name="email"><br>
Message:<br><textarea rows="5" name="message" cols="30"></textarea><br>
<div class="g-recaptcha" data-sitekey="6LemsnMUAAAAAF33gq-5IdYZI5yUAWU9C0FRgzD6"></div>
<input type="submit" name="submit" value="Submit">


</form>
            </div>
        </div>
    </div>
</section>
<!-- END OF SECTION LINKS AND BEGINING OF FOOTER -->

<?php
/**
 * Requesting the footer from includes folder
 */
require_once("includes/footer.php");

?>